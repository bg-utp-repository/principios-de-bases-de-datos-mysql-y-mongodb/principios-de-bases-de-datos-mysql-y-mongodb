use bd_test; 


drop table if exists bd_test.cliente;

CREATE TABLE bd_test.cliente (
    id_cliente int NOT NULL,
	nombre varchar(200) NOT NULL,
	apellido varchar (64) NULL,
    cedula varchar(64) NOT NULL,
	sexo char(1) NULL,
    fecha_nacimiento datetime NOT NULL,
    PRIMARY KEY (id_cliente),
    UNIQUE INDEX idx_unique_cedula (cedula)
    );

drop table if exists bd_test.hobbie; 
create table bd_test.hobbie (
id_hobbie int not null AUTO_INCREMENT, 
descripcion varchar(64),
primary key (id_hobbie) 
);

drop table if exists bd_test.cliente_hobbie;
create table bd_test.cliente_hobbie (
id_cliente int not null, 
id_hobbie int not null, 
primary key (id_cliente, id_hobbie));




