USE bd_test; 
drop procedure if exists sp_cliente_hobbie;
DELIMITER //
create procedure sp_cliente_hobbie (
IN i_operacion char(2),
IN i_id_cliente int,
IN i_id_hobbie int,
IN i_id_hobbie_new int,
OUT o_return int
)
BEGIN


if i_operacion = 'C1' then 
insert into cliente_hobbie (id_cliente, id_hobbie) values (i_id_cliente, i_id_hobbie);
end if;

if i_operacion = 'R1' then 
select id_cliente, 
       id_hobbie
from bd_test.cliente_hobbie 
where id_cliente = i_id_cliente;
end if;

if i_operacion = 'U1' then 
update  bd_test.cliente_hobbie
set id_hobbie = i_id_hobbie_new
where id_cliente = i_id_cliente
  and id_hobbie = i_id_hobbie; 
end if;

if i_operacion = 'D1' then 
delete from bd_test.cliente_hobbie
where id_cliente = i_id_cliente
  and id_hobbie = i_id_hobbie;
end if;

END//

DELIMITER ;

